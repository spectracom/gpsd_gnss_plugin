#include <gps.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>

#include <oscillatord/log.h>
#include <oscillatord/gnss.h>
#include <oscillatord/config.h>
#include <oscillatord/gnss_factory.h>
#include <oscillatord/utils.h>

#define FACTORY_NAME "gpsd"

#define GPS_JSON_RESPONSE_MAX 10240
#define TIMEOUT_USEC 1000
#define GPS_TO_TAI_TIME 19

static unsigned int gpsd_gnss_index;

struct gpsd_gnss {
	struct gnss gnss;
	int fd_clock;
	bool session_open;
	struct gps_data_t data;
};

/*
 * Compute TAI time from UTC one
 * Computing from UTC is not ideal but gps_data_t does not
 * have tow of GpsWeek to compute from GPS Time
 */
static time_t gpsd_get_next_fix_tai_time(struct gnss *gnss)
{
	struct gpsd_gnss *gpsd;
	time_t utc_time = 0;
	int leap_seconds = 0;

	gpsd = container_of(gnss, struct gpsd_gnss, gnss);

	if (!(gpsd->data.set & TIME_SET))
		return 0;
	leap_seconds = gpsd->data.leap_seconds;
	utc_time = gpsd->data.fix.time.tv_sec;

	return utc_time + leap_seconds + GPS_TO_TAI_TIME;
}

/*
 * Check if data is set and Fix mode is good enough to use data
 */
static enum gnss_state gnss_data_valid(const struct gpsd_gnss *gpsd)
{
	if (!(gpsd->data.set & PACKET_SET)) {
		log_trace("gps data not set");
		return GNSS_WAITING;
	}

	// TODO: Add check of fix status 
	// if (gpsd->data.fix.status < STATUS_FIX) {
	// 	log_trace("No gps fix");
	// 	return GNSS_INVALID;
	// }

	if (gpsd->data.fix.mode < MODE_2D) {
		log_trace("Fix mode < 2D");
		return GNSS_INVALID;
	}

	return GNSS_VALID;
}

/*
 * Return GNSS receiver  data state (valid, invalid, unknown)
 */
static enum gnss_state gpsd_get_valid(struct gnss *gnss)
{
	struct gpsd_gnss *gpsd;
	__attribute__((cleanup(string_cleanup))) char *message;
	enum gnss_state valid = GNSS_ERROR;

	gpsd = container_of(gnss, struct gpsd_gnss, gnss);

	/* Heap alloc to reduce stack size and fix coverity warning */
	message = malloc(GPS_JSON_RESPONSE_MAX*4);
	while (true) {
		if (!gps_waiting(&gpsd->data, TIMEOUT_USEC)) {
			if (errno != 0) {
				log_error("gps_waiting: %d", errno);
				return GNSS_ERROR;
			}
			return valid;
		}

		if (gps_read(&gpsd->data, message,
				GPS_JSON_RESPONSE_MAX*4) == -1) {
			if (errno == 0)
				log_error("gps_read: the socket to the daemon"
					" has closed or the shared-memory"
					" segment was unavailable");
			else
				log_error("gps_read: %d", errno);
			return GNSS_ERROR;
		}
		valid = gnss_data_valid(gpsd);
	}
	return valid;
}

static void gnss_cleanup(struct gpsd_gnss *gpsd)
{
	if (!gpsd->session_open)
		return;

	log_debug("Closing gpsd session");

	gps_stream(&gpsd->data, WATCH_DISABLE, NULL);
	if (gps_close(&gpsd->data) == -1)
		log_error("gps_close %d", errno);

	gpsd->session_open = false;
}

static void gpsd_gnss_destroy(struct gnss **gnss)
{
	struct gnss *g;
	struct gpsd_gnss *gt;

	if (gnss == NULL || *gnss == NULL)
		return;

	g = *gnss;
	gt = container_of(g, struct gpsd_gnss, gnss);
	gnss_cleanup(gt);
	memset(g, 0, sizeof(*g));
	free(g);
	*gnss = NULL;
}

static struct gnss *gpsd_gnss_new(struct config *config, int fd_clock)
{
	struct gpsd_gnss *gpsd;
	struct gnss *gnss;
	int ret;
	const char *gnss_device_tty;
	const char *gpsd_addr;
	const char *gpsd_port;

	gpsd = calloc(1, sizeof(*gpsd));
	if (gpsd == NULL)
		return NULL;
	gnss = &gpsd->gnss;

	// Not used for now
	gpsd->fd_clock = fd_clock;

	gnss_device_tty = config_get(config, "gnss-device-tty");
	if (gnss_device_tty == NULL) {
		log_error("device-tty not defined in config %s", config->path);
		goto error;
	}

	log_info("GNSS device tty %s", gnss_device_tty);

	gpsd_addr = config_get(config, "gpsd-addr");
	if (gpsd_addr == NULL) {
		log_error("gpsd-addr not defined in config %s", config->path);
		goto error;
	}

	gpsd_port = config_get(config, "gpsd-port");
	if (gpsd_port == NULL) {
		log_error("gpsd-port not defined in config %s", config->path);
		goto error;
	}

	ret = gps_open(gpsd_addr, gpsd_port, &gpsd->data);
	if (ret != 0) {
		log_error("gps_open %s: ", gps_errstr(ret));
		goto error;
	}

	gpsd->session_open = true;
	log_debug("Starting gpsd session");

	if (gps_stream(&gpsd->data, WATCH_ENABLE | WATCH_NMEA | WATCH_DEVICE | WATCH_JSON | WATCH_SCALED | WATCH_NEWSTYLE,
	    (void *)gnss_device_tty) != 0) {
		log_error("gps_stream", errno);
		goto error;
	}

	gnss_factory_init(FACTORY_NAME, gnss, FACTORY_NAME "-%d", gpsd_gnss_index);
	gpsd_gnss_index++;
	log_debug("Instantiated " FACTORY_NAME " gnss");

	return gnss;
error:
	gpsd_gnss_destroy(&gnss);
	return NULL;
}

static const struct gnss_factory gpsd_gnss_factory = {
	.class = {
			.name = FACTORY_NAME,
			.get_valid = gpsd_get_valid,
			.get_next_fix_tai_time = gpsd_get_next_fix_tai_time,
	},
	.new = gpsd_gnss_new,
	.destroy = gpsd_gnss_destroy,
};


static void __attribute__((constructor)) gpsd_gnss_constructor(void)
{
	int ret;

	ret = gnss_factory_register(&gpsd_gnss_factory);
	if (ret < 0)
		log_error("gnss_factory_register", ret);
};